## Notes

**Build a shopping cart of groceries**

---

Properties:

1. Console application
2. Built using Vidual Studio 2017
3. .NET Framework Version 4.5

---

**Description**

Using a few key concepts of Object Oriented Programming, this app builds a grocery shopping cart and outputs its contents to the user.

---

**Gotcha**

This shopping cart counts items as you would expect an online shopping cart to count them, not as one might count them in a physical shopping cart. When you add 6 of one item to the cart, the cart will show that you have one item in it and will display the selected quantity of that item. It will then calculate the purchase total using both.

---