﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Sample1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Complete the solution to make the following code work.                       
            
            Cart groceryCart = new Cart() { TypeOfCart = TypeOfCart.Grocery }; ;
            groceryCart.AddItem(new Dairy().WithDescription("Gallon of Milk").WithUnitPrice(3.50m).WithQuantity(2)); //7
            groceryCart.AddItem(new Dairy().WithDescription("Heavy Cream").WithUnitPrice(4.59m)); //4.59
            groceryCart.AddItem(new Dairy().WithDescription("Yogurt").WithUnitPrice(.59m).WithQuantity(6)); //3.54
            groceryCart.AddItem(new Meat().WithDescription("Hamburger").WithUnitPrice(2.96m).WithWeight(3.5m)); //10.36

            Console.WriteLine($"{groceryCart.NumberOfItems} items at a total price of ${groceryCart.TotalPrice()}");
            Console.WriteLine($"The {groceryCart.TypeOfCart} Cart contains the following {groceryCart.Contents()}");
            Console.ReadLine();

        }
    }

    internal class Cart
    {
        public TypeOfCart TypeOfCart { get; set; }
        public int NumberOfItems
        {
            get
            {
                return this.Items.Count;
            }
        }
        public List<IGroceryItem> Items { get; set; } = new List<IGroceryItem>();

        internal void AddItem(IGroceryItem item)
        {
            this.Items.Add(item);
        }

        internal string TotalPrice()
        {
            return this.Items.Count > 0 ? this.Items.Select(i => i.TotalPrice).ToList().Sum().ToString("n2") : "0";
        }

        internal string Contents()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("\nQTY.\t");
            sb.Append("DESCRIPTION");
            if (this.Items.Count > 0)
            {
                this.Items.ForEach(i =>
                {
                    sb.Append("\n ");
                    sb.Append(i.Quantity);
                    sb.Append("\t");
                    sb.Append(i.Description);
                });
            }
            else
            {
                sb.Append("\nThere are no items in the cart");
            }

            return sb.ToString();
        }
    }

    internal enum TypeOfCart
    {
        Grocery
    }

    //Provided to allow upside down inheritance
    public interface IGroceryItem
    {
        decimal UnitPrice { get; }
        string Description { get; }
        int Quantity { get; }
        decimal TotalPrice { get; }
    }

    //Generic item can inherit this
    internal abstract class GroceryItem<T> : IGroceryItem
    {
        public decimal UnitPrice { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; } = 1;
        public virtual decimal TotalPrice
        {
            get
            {
                return this.Quantity * this.UnitPrice;
            }
        }

        public abstract T Self();

        public T WithDescription(string description)
        {
            this.Description = description;
            return Self();
        }

        public T WithUnitPrice(decimal price)
        {
            this.UnitPrice = price;
            return Self();
        }

        public T WithQuantity(int quantity)
        {
            this.Quantity = quantity;
            return Self();
        }
    }

    //Base can be aware of derived class
    internal class Dairy : GroceryItem<Dairy> {
        public override Dairy Self()
        {
            return this;
        }
    }

    //Base can be aware of derived class
    internal class Meat : GroceryItem<Meat>
    {
        public decimal Weight { get; set; }
        public override Meat Self()
        {
            return this;
        }
        public override decimal TotalPrice
        {
            get
            {
                return this.UnitPrice * this.Weight * this.Quantity;
            }
        }

        public Meat WithWeight(decimal weight)
        {
            this.Weight = weight;
            return this;
        }
    }
}
